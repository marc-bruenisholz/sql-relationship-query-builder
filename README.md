# sql-relationship-query-builder

It wraps *sql-select-query-builder* and provides a method
to add SQL relationships using *sql-relationship-models* that are
converted into SQL joins when building the SQL query string.
Delegates SQL query string building to *sql-select-query-builder*.

This repository is part of the project SQLQueriesByPropertyPaths.