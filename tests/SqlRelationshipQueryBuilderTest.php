<?php
/** @noinspection SqlResolve */

/** @noinspection SqlNoDataSourceInspection */


namespace ch\_4thewin\SqlRelationshipQueryBuilder;

use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\ManyToOne;
use ch\_4thewin\SqlRelationshipModels\OneToMany;
use ch\_4thewin\SqlRelationshipModels\OneToOne;
use ch\_4thewin\SqlSelectModels\Arguments\IntArgument;
use ch\_4thewin\SqlSelectModels\Arguments\StringArgument;
use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqlSelectModels\Table;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\ColumnExpression;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_EQ;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_GT;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_IN;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_LIKE;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\IdCondition;
use PHPUnit\Framework\TestCase;

class SqlRelationshipQueryBuilderTest extends TestCase
{
    public function testOneToOneWithSearchCondition()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('owningTableName1', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    (new OneToOne(
                        $otherTable,
                        'foreignKeyColumnName1','string',
                        $fromTable,
                        true
                    ))->setSearchCondition(
                        new _EQ(
                            new ColumnExpression($otherTable, 'columnName', 'string'),
                            new StringArgument('columnValue')
                        )
                    )->setJoinType('INNER')
                );

        self::assertEquals(
            "SELECT * FROM `fromTableName` JOIN `owningTableName1` ".
            "ON `fromTableName`.`id` = `owningTableName1`.`foreignKeyColumnName1` AND `owningTableName1`.`columnName` = ?",
            $builder->build()->getQueryString()
        );
    }

    public function testOneToOne()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('owningTableName1', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    new OneToOne(
                        $otherTable,
                        'foreignKeyColumnName1','string',
                        $fromTable,
                        true
                    )
                );

        self::assertEquals(
            "SELECT * FROM `fromTableName` LEFT JOIN `owningTableName1` ON `fromTableName`.`id` = `owningTableName1`.`foreignKeyColumnName1`",
            $builder->build()->getQueryString()
        );
    }

    public function testOneToManyWithSearchCondition()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('owningTableName1', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    (new OneToMany(
                        $otherTable,
                        'foreignKeyColumnName1','string',
                        $fromTable
                    ))->setSearchCondition(
                        new _GT(
                            new ColumnExpression($otherTable, 'columnName', 'string'),
                            new IntArgument(1)
                        )
                    )->setJoinType('INNER')
                );

        self::assertEquals(
            "SELECT * FROM `fromTableName` JOIN `owningTableName1` ON ".
            "`fromTableName`.`id` = `owningTableName1`.`foreignKeyColumnName1` AND `owningTableName1`.`columnName` > ?",
            $builder->build()->getQueryString()
        );
    }

    public function testOneToMany()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('owningTableName1', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    new OneToMany(
                        $otherTable,
                        'foreignKeyColumnName1','string',
                        $fromTable
                    )
                );

        self::assertEquals(
            "SELECT * FROM `fromTableName` LEFT JOIN `owningTableName1` ON `fromTableName`.`id` = `owningTableName1`.`foreignKeyColumnName1`",
            $builder->build()->getQueryString()
        );
    }

    public function testManyToManyWithSearchConditions()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        // TODO several primary key columns possible!
        $intermediaryTable = new Table('intermediaryTable', 'id', 'string');
        $otherTable = new Table('otherTable', 'id', 'string');
        $manyToMany = new ManyToMany(
            $fromTable,
            $intermediaryTable,
            'firstForeignKeyColumnName','string',
            'secondForeignKeyColumnName','string',
            $otherTable
        );
        // Set ID search condition on intermediary table join
        $manyToMany->getOneToMany()->setSearchCondition(
            new IdCondition(
                new ColumnExpression($intermediaryTable, 'secondForeignKeyColumnName', 'string'),
                [new StringArgument('id1')]
            )
        )->setJoinType('INNER');
        // Set other search conditions on right table join
        $manyToMany->getManyToOne()->setSearchCondition(
            new _LIKE(
                new ColumnExpression($otherTable, 'columnName', 'string'),
                new StringArgument('%name%')
            )
        )->setJoinType('INNER');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    $manyToMany
                );

        self::assertEquals(
            'SELECT * FROM `fromTableName` JOIN `intermediaryTable` '.
            'ON `fromTableName`.`id` = `intermediaryTable`.`firstForeignKeyColumnName` '.
            'AND `intermediaryTable`.`secondForeignKeyColumnName` = ? '.
            'JOIN `otherTable` ON `otherTable`.`id` = `intermediaryTable`.`secondForeignKeyColumnName` AND `otherTable`.`columnName` LIKE ?',
            $builder->build()->getQueryString()
        );
    }

    public function testManyToMany()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        // TODO several primary key columns possible!
        $intermediaryTable = new Table('intermediaryTable', 'id', 'string');
        $otherTable = new Table('otherTable', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    new ManyToMany(
                        $fromTable,
                        $intermediaryTable,
                        'firstForeignKeyColumnName','string',
                        'secondForeignKeyColumnName','string',
                        $otherTable
                    )
                );

        self::assertEquals(
            "SELECT * FROM `fromTableName` LEFT JOIN `intermediaryTable` ON ".
            "`fromTableName`.`id` = `intermediaryTable`.`firstForeignKeyColumnName` JOIN `otherTable` ON ".
            "`otherTable`.`id` = `intermediaryTable`.`secondForeignKeyColumnName`",
            $builder->build()->getQueryString()
        );
    }

    public function testManyToOneWithSearchConditions()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('otherTableName', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    (new ManyToOne(
                        $fromTable,
                        'foreignKeyColumnName1','string',
                        $otherTable
                    ))->setSearchCondition(
                        new _IN(
                            new ColumnExpression($otherTable, 'columnName', 'string'),
                            [new StringArgument('value1'), new StringArgument('value2')]
                        )
                    )->setJoinType('INNER')
                );

        self::assertEquals(
            'SELECT * FROM `fromTableName` JOIN `otherTableName` ON '.
            '`otherTableName`.`id` = `fromTableName`.`foreignKeyColumnName1` AND `otherTableName`.`columnName` IN (?,?)',
            $builder->build()->getQueryString()
        );
    }

    public function testManyToOne()
    {
        $fromTable = new Table('fromTableName', 'id', 'string');
        $otherTable = new Table('otherTableName', 'id', 'string');
        $builder =
            (new SqlRelationshipQueryBuilder(
                (new Select($fromTable))
            ))
                ->addRelationship(
                    new ManyToOne(
                        $fromTable,
                        'foreignKeyColumnName1','string',
                        $otherTable
                    )
                );

        self::assertEquals(
            'SELECT * FROM `fromTableName` LEFT JOIN `otherTableName` ON `otherTableName`.`id` = `fromTableName`.`foreignKeyColumnName1`',
            $builder->build()->getQueryString()
        );

        self::assertEquals(
            new ManyToOne(
                $fromTable,
                'foreignKeyColumnName1','string',
                $otherTable
            ),
            $builder->getRelationships()[0]
        );

        self::assertFalse($builder->isIgnoringRelationshipSearchConditions());
        $builder->setIsIgnoringRelationshipSearchConditions(true);
        self::assertTrue($builder->isIgnoringRelationshipSearchConditions());
    }

}
