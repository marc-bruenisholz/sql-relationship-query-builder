<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlRelationshipQueryBuilder;

use ch\_4thewin\SqlRelationshipModels\ManyToMany;
use ch\_4thewin\SqlRelationshipModels\Relationship;
use ch\_4thewin\SqlRelationshipModels\SingleCardinalityRelationship;
use ch\_4thewin\SqlSelectModels\Join;
use ch\_4thewin\SqlSelectModels\Select;
use ch\_4thewin\SqlSelectQueryBuilder\SqlSelectQueryBuilder;
use ch\_4thewin\SqppDbConnectorInterface\SqlParameterizedSelectQuery;
use ch\_4thewin\SqppSqlExpressionBuildingBlocks\Conditions\_ANDs;

/**
 * The class SqlRelationshipSelectQueryBuilder creates sql joins from
 * added relationships and creates a query using the SqlSelectQueryBuilder.
 * @package ch\_4thewin\SqlRelationshipSelectQueryBuilder
 */
class SqlRelationshipQueryBuilder extends SqlSelectQueryBuilder
{
    /** @var bool */
    protected bool $isIgnoringRelationshipSearchConditions = false;

    /**
     * @var Relationship[]
     */
    protected array $relationships = [];

    /**
     * @param Select $select
     */
    public function __construct(Select $select)
    {
        parent::__construct($select);
    }

    public function addRelationship(Relationship $relationship): self
    {
        $this->relationships[] = $relationship;
        return $this;
    }

    /**
     * @return Relationship[]
     */
    public function getRelationships(): array
    {
        return $this->relationships;
    }

    public function build(): SqlParameterizedSelectQuery
    {
        $relationshipJoins = [];
        foreach ($this->relationships as $relationship) {
            if ($relationship instanceof SingleCardinalityRelationship) {
                $relationshipJoins[] = $this->createJoinFromSingleCardinalityRelationship($relationship);
            } else {
                // TODO never used because only SingleCardinalityRelationship have been added. Maybe remove.
                /** @var ManyToMany $manyToMany */
                $manyToMany = $relationship;

                $relationshipJoins[] = $this->createJoinFromSingleCardinalityRelationship(
                    $manyToMany->getOneToMany()
                );
                $relationshipJoins[] = $this->createJoinFromSingleCardinalityRelationship(
                    $manyToMany->getManyToOne(),
                    true
                );
            }
        }

        $unalteredJoins = $this->getSelect()->getJoins();

        // TODO there has been repeatedly unexpected behaviour after code changes
        //  because the joins are bound to the select so late.
        //  Investigate whether code can be moved into property-path-tree-queries-builder
        //  and joins created immediately.
        //  A problem is the flag isIgnoringRelationshipSearchConditions.
        // For now, check if there is a sub select. If so, add the joins to the sub select
        $fromClause = $this->getSelect()->getFromClause();
        if($fromClause instanceof Select) {
            $fromClause->setJoins(array_merge($unalteredJoins, $relationshipJoins));
        } else {
            $this->getSelect()->setJoins(array_merge($unalteredJoins, $relationshipJoins));
        }
        $build = parent::build();
        $this->getSelect()->setJoins($unalteredJoins);
        return $build;
    }


    private function createJoinFromSingleCardinalityRelationship(
        SingleCardinalityRelationship $relationship,
        bool $alwaysInner = false
    ): Join {
        $searchCondition = $this->isIgnoringRelationshipSearchConditions ? null : $relationship->getSearchCondition();
        $accessControlCondition = $relationship->getAccessControlCondition();
        $condition = null;
        if($accessControlCondition !== null) {
            if($searchCondition === null) {
                $condition = $accessControlCondition;
            } else {
                $condition = new _ANDs();
                $condition->addCondition($accessControlCondition);
                $condition->addCondition($searchCondition);
            }
        } else {
            $condition = $searchCondition;
        }

        return new Join(
            $relationship->getOwningTable(),
            $relationship->getForeignKeyColumnName(),
            $relationship->getInverseTable(),
            $relationship->isFromInverseToOwningTable(),
            $condition,
            $alwaysInner ? 'INNER' : $relationship->getJoinType()
        );
    }

    /**
     * @return bool
     */
    public function isIgnoringRelationshipSearchConditions(): bool
    {
        return $this->isIgnoringRelationshipSearchConditions;
    }

    public function setIsIgnoringRelationshipSearchConditions(bool $isIgnoringRelationshipSearchConditions): self
    {
        $this->isIgnoringRelationshipSearchConditions = $isIgnoringRelationshipSearchConditions;
        return $this;
    }
}